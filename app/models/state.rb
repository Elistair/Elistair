# Model for US states
class State < ApplicationRecord
  validates :name, uniqueness: true
end
