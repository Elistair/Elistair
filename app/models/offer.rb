# Model that represents a property listing offer
class Offer < ApplicationRecord
  enum deed_type: [
    :warranty,
    :special_warranty,
    :quit_claim,
    :bargain_and_sale,
    :grant
  ]

  has_one :user
  has_one :listing
end
