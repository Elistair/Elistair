# Model for managing BackgroundChecks from Checkr
class BackgroundCheck < ApplicationRecord
  has_one :user
end
