# Model for a Property Showing
class Showing < ApplicationRecord
  enum showing_type: [:open, :closed]

  has_one :user
  has_one :listing
end
