# Listing model to represent a property listing
class Listing < ApplicationRecord
  enum unit_type: [:feet, :meters, :acres]
  enum property_type: [
    :single_family,
    :condominuim,
    :townhouse,
    :multi_family,
    :land,
    :other
  ]

  has_one :user
  has_many :offers
  has_many :showings

  validates :address_line_one, :city, :state_id, :zip_code, presence: true
end
