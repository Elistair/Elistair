# InfoController for rendering a few static assets
# that need ruby
class InfoController < ApplicationController
  def license; end

  def contact; end

  def privacy; end

  def terms; end
end
