require 'test_helper'

class StateTest < ActiveSupport::TestCase
  test 'it should require a unique name' do
    mn = State.new(name: 'Minnesota')
    assert_not mn.save # already exists from fixtures
    mars = State.new(name: 'Mars')
    assert mars.save
  end
end
