require 'test_helper'

class ListingTest < ActiveSupport::TestCase
  test 'it should not save if an street, city, state, zip_code are absent' do
    listing = Listing.new
    assert listing.save == false
  end

  test 'it should save if an address is present' do
    listing = Listing.new
    listing.address_line_one = '123 Main Street'
    listing.city             = 'Minneapolis'
    listing.state_id         = states(:Minnesota).id
    listing.zip_code         = '55402'
    assert listing.save!
  end
end
