Rails.application.routes.draw do

  # Static pages
  %w[license contact privacy terms].each do |page|
    get page, controller: 'info', action: page
  end

  devise_for :users
  resources :listings
  root 'home#index'
end
