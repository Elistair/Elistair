FROM ruby:2.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs libxml2-dev libxslt1-dev libqt4-webkit libqt4-dev xvfb
ENV APP_HOME /var/www/elistair
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
ADD Gemfile* $APP_HOME/
RUN gem install bundler
RUN bundle install
ADD . $APP_HOME
RUN bundle exec rake assets:precompile
