class CreateStates < ActiveRecord::Migration[5.0]
  def change
    create_table :states do |t|
      t.string :name, index: true
    end

    change_table :listings do |t|
      t.remove :state
      t.integer :state_id, foreign_key: true
    end
  end
end
