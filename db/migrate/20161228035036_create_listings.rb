class CreateListings < ActiveRecord::Migration[5.0]
  def change
    enable_extension 'hstore'

    create_table :listings do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string  :address_line_one
      t.string  :address_line_two
      t.string  :address_line_three
      t.string  :city
      t.string  :state
      t.string  :county
      t.string  :zip_code
      t.integer :price
      t.integer :beds
      t.integer :baths
      t.integer :listing_size
      t.integer :listing_size_unit
      t.integer :lot_size
      t.integer :lot_size_unit
      t.text    :description
      t.string  :style
      t.integer :property_type
      t.integer :year_build
      t.string  :community
      t.integer :mls_number
      t.hstore  :details

      t.timestamps
    end
  end
end
