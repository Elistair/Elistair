class CreateShowings < ActiveRecord::Migration[5.0]
  def change
    create_table :showings do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :listing, index: true, foreign_key: true
      t.datetime   :start_time
      t.integer    :duration
      t.integer    :showing_type, index: true

      t.timestamps
    end
  end
end
