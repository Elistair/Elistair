class SimplifyUnitTypeEnums < ActiveRecord::Migration[5.0]
  change_table :listings do |t|
    t.remove :listing_size_unit
    t.remove :lot_size_unit
    t.integer :unit_type
  end
end
