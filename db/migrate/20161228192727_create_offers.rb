class CreateOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :offers do |t|
      t.belongs_to :listing, index: true, foreign_key: true
      t.belongs_to :user,    index: true, foreign_key: true
      t.integer    :price
      t.integer    :price_unit
      t.string     :terms,              :array => true, :default => []
      t.datetime   :closing_date
      t.integer    :earnest_money
      t.string     :adjusted_expenses_method
      t.string     :provisions,         :array => true, :default => []
      t.integer    :deed_type
      t.string     :other_requirements, :array => true, :default => []
      t.datetime   :expires_at
      t.hstore     :contingencies

      t.timestamps
    end
  end
end
