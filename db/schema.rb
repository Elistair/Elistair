# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161229202425) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "background_checks", force: :cascade do |t|
    t.integer  "user_id"
    t.jsonb    "results"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_background_checks_on_user_id", using: :btree
  end

  create_table "listings", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "address_line_one"
    t.string   "address_line_two"
    t.string   "address_line_three"
    t.string   "city"
    t.string   "county"
    t.integer  "state_id"
    t.string   "zip_code"
    t.integer  "price"
    t.integer  "beds"
    t.integer  "baths"
    t.integer  "listing_size"
    t.integer  "lot_size"
    t.integer  "unit_type"
    t.text     "description"
    t.string   "style"
    t.integer  "property_type"
    t.integer  "year_build"
    t.string   "community"
    t.integer  "mls_number"
    t.jsonb    "details"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["user_id"], name: "index_listings_on_user_id", using: :btree
  end

  create_table "offers", force: :cascade do |t|
    t.integer  "listing_id"
    t.integer  "user_id"
    t.integer  "price"
    t.integer  "price_unit"
    t.string   "terms",                    default: [],              array: true
    t.datetime "closing_date"
    t.integer  "earnest_money"
    t.string   "adjusted_expenses_method"
    t.string   "provisions",               default: [],              array: true
    t.integer  "deed_type"
    t.string   "other_requirements",       default: [],              array: true
    t.datetime "expires_at"
    t.hstore   "contingencies"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["listing_id"], name: "index_offers_on_listing_id", using: :btree
    t.index ["user_id"], name: "index_offers_on_user_id", using: :btree
  end

  create_table "showings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "listing_id"
    t.datetime "start_time"
    t.integer  "duration"
    t.integer  "showing_type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["listing_id"], name: "index_showings_on_listing_id", using: :btree
    t.index ["showing_type"], name: "index_showings_on_showing_type", using: :btree
    t.index ["user_id"], name: "index_showings_on_user_id", using: :btree
  end

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.index ["name"], name: "index_states_on_name", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

  add_foreign_key "background_checks", "users"
  add_foreign_key "listings", "states"
  add_foreign_key "listings", "users"
  add_foreign_key "offers", "listings"
  add_foreign_key "offers", "users"
  add_foreign_key "showings", "listings"
  add_foreign_key "showings", "users"
end
